require_relative 'app/reader'

class Main

	logfile = "logs/log.txt"

	log_reader = Reader.new

	urls, statuses = log_reader.filereader(logfile)

	puts "\nMost requested urls:\n"
	3.times do |i|
		puts "#{urls.keys[i]} - #{urls.values[i]}"
	end

	puts "\nStatus:\n"
	statuses.each do |s|
		puts "#{s[0]} - #{s[1]}"
	end

end