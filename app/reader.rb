require "uri"

class Reader

	def filereader(logfile)

		url_map = Hash.new 0
		status_map = Hash.new 0

		raise 'You should not pass! File not found, try again.' if !File.exist?(logfile)

		File.open(logfile, "r") do |f|
			f.each_line do |line|
				http_status = line.scan(/response_status="([^"]\d+)"/).to_s.tr('[["]]', '')
				webhook_url = line.scan(/request_to="([^"]*)"/).to_s.tr('[["]]', '')
				
				if(http_status != "" && webhook_url != "")
					url_map[webhook_url] += 1
					status_map[http_status] += 1
				end

			end
		end
		sort(url_map, status_map)
	end

	def sort(urls, statuses)
		sorted_statuses = statuses.sort_by {|k,v| v}.reverse.to_h
		sorted_urls = urls.sort_by {|k,v| v}.reverse.to_h

		return sorted_urls, sorted_statuses
	end

end