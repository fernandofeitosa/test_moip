require 'test/unit'
require_relative '../app/reader'

class TestReader < Test::Unit::TestCase

	def setup
		logfile = "../logs/log_test.txt"
		log_reader = Reader.new
		@urls, @status = log_reader.filereader(logfile)
	end
	
	def test_count_urls
		assert_equal @urls["https://woodenoyster.com.br"], 6 
		assert_equal @urls["https://notoriouslonesome.com"], 5
		assert_equal @urls["https://grimpottery.net.br"], 4
	end

	def test_count_status
		assert_equal @status["200"], 8
		assert_equal @status["500"], 8
		assert_equal @status["201"], 1
	end

	def test_sorting
		assert_equal @urls.keys.first, "https://woodenoyster.com.br"
		assert_equal @urls.values.first, 6
		assert_equal @urls.keys.last, "https://endlessiron.com.br"
		assert_equal @urls.values.last, 1

		assert_equal @status.keys.first, "400"
		assert_equal @status.values.first, 10
		assert_equal @status.keys.last, "201"
		assert_equal @status.values.last, 1
	end

	def test_file_not_found
		logfile = "X-file.txt"
		log_reader = Reader.new
		assert_raise RuntimeError do
			log_reader.filereader(logfile)
		end
	end
end